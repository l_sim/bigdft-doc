docker volume create dbvolume
docker run -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=wordpress -e MYSQL_USER=wordpress -e MYSQL_PASSWORD=wordpress --name wordpressdb  -v dbvolume:/var/lib/mysql  -d mysql:5.7
docker run --name wordpress --link wordpressdb:mysql -p 8080:80 -v $PWD/wordpress:/var/www/html/ -d wordpress

