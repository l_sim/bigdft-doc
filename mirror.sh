git rm -rf public-wp/*
wget --mirror --convert-links  --adjust-extension --page-requisites  --retry-connrefused  --exclude-directories=comments --execute robots=off $1
mkdir public-wp
mv $1/* public-wp/.
rmdir $1
find ./public-wp/ -type f -exec sed -i -e "s|http://$1|https://$2|g" {} \;
find ./public-wp/ -type f -exec sed -i -e "s|http://$2|https://$2|g" {} \;
git add public-wp/*
git commit -m "new snapshot"
git push origin devel
